# OpenML dataset: AAPL_stock_price_2021_2022_2

https://www.openml.org/d/43882

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Apple stock price of each work day since January 1st 2021. Contains highest price, lowest price,     open, close, volume and adjusted close

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43882) of an [OpenML dataset](https://www.openml.org/d/43882). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43882/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43882/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43882/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

